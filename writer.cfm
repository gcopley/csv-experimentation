<cfscript>

    data = [
        {
            reviewDate: "1985/01/21",
            author: "Douglas Adams",
            isbn: "0345391802",
            discountedPrice: 5.95
        },
        {
            reviewDate: "1990/01/12",
            author: "Douglas ""Hofstadter",
            isbn: "0465026567",
            discountedPrice: 9.95
        }        

    ];

    try {

        csv = createObject("java", "com.csvreader.CsvWriter").init(fileName=expandPath("data_write.csv"));
        
        // Headers
        csv.write("REVIEW_DATE");
        csv.write("AUTHOR");
        csv.write("ISBN");
        csv.write("DISCOUNTED_PRICE");
        csv.endRecord();
        
        // Entries
        for (entry in data) {
            csv.write(entry.reviewDate);
            csv.write(entry.author);
            csv.write(entry.isbn);
            csv.write(entry.discountedPrice);
            csv.endRecord();
        }

    } catch (any e) {
        
        dump(e);

    } finally {
      
        try {
        
            csv.close();
        
        } catch (any e) {

        }

        dump(data);
    }


</cfscript>