<cfscript>
    data = [];
    
    try {
    
        csv = createObject("java", "com.csvreader.CsvReader").init(fileName=expandPath("data.csv"), delimiter=",");
        csv.readHeaders();

        while (csv.readRecord()) {
            data.append({
                reviewDate: csv.get("REVIEW_DATE"),
                author: csv.get("AUTHOR"),
                isbn: csv.get("ISBN"),
                discountedPrice: csv.get("DISCOUNTED_PRICE")
            });
        }

    } finally {
        try {
            csv.close();
        } catch (any e) {

        }

        dump(data);
    }
</cfscript>
