CSV Testing
========================

Frustrated with a lack of native CSV reading and writing capabilities within Railo,
and performance issues with CFC alternatives, I set out to find an easy Java library
for reading and writing CSV data.

I've created this project for my personal use and testing, however I hope that it can 
be of some help to someone else.
 
Java CSV
------------------------

CSVReader.com provides an open source CSV library which I've used in this project.
http://www.csvreader.com/java_csv.php

Railo Installation
------------------------

[1]: Download Java CSV
[2]: Extract the file javacsv.jar into your WEB-INF/railo/lib folder.
[3]: You will likely need to restart Railo servlet.